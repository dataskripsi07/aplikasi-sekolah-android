package com.apps.pendaftaransekolah.ui.home

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.apps.pendaftaransekolah.R
import com.apps.pendaftaransekolah.api.ApiEndPoint
import com.apps.pendaftaransekolah.api.Resource
import com.apps.pendaftaransekolah.data.response.LoginResp
import com.apps.pendaftaransekolah.data.response.LulusResp
import kotlinx.coroutines.launch

class HomeViewModel (
    val api: ApiEndPoint,
    val context : Context
) : ViewModel() {

    val kelulusanResp: MutableLiveData<Resource<LulusResp>> = MutableLiveData()
    fun getKelulusan(id_user: String, id_pendaftaran: String) = viewModelScope.launch {
        kelulusanResp.value = Resource.Loading()
        val dataApi = api.getKelulusan(idUser = id_user, idPendaftaran = id_pendaftaran)
        try {
            if (dataApi.code() == 401) {
                kelulusanResp.value = Resource.Error("Token Unauthorized atau akun sedang tidak aktif!")
            } else {
                if (dataApi.body()?.error.equals("true")) {
                    kelulusanResp.value = Resource.Success( dataApi.body()!! )
                } else {
                    kelulusanResp.value = Resource.Error( dataApi.body()?.message.toString() )
                }
            }

        } catch (e: Exception) {
            if(dataApi.code() == 500) {
                kelulusanResp.value = Resource.Error(context.getString(R.string.error_server))
            } else {
                kelulusanResp.value = Resource.Error( e.message.toString() )
            }
        }
    }

}