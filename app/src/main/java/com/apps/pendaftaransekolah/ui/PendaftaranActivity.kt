package com.apps.pendaftaransekolah.ui

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.DatePicker
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.apps.driverasrikatara.helper.LoadingScreen
import com.apps.pendaftaransekolah.R
import com.apps.pendaftaransekolah.api.ApiService
import com.apps.pendaftaransekolah.api.Resource
import com.apps.pendaftaransekolah.data.constants.Constants
import com.apps.pendaftaransekolah.data.preferences.Preferences
import com.apps.pendaftaransekolah.databinding.ActivityPendaftaranBinding
import com.apps.pendaftaransekolah.databinding.ActivitySplashBinding
import com.apps.pendaftaransekolah.helper.URIPathHelper
import com.apps.pendaftaransekolah.ui.auth.LoginActivity
import com.apps.pendaftaransekolah.ui.auth.LoginViewModel
import com.apps.pendaftaransekolah.ui.auth.LoginViewModelFactory
import com.apps.pendaftaransekolah.ui.home.HomeActivity
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import timber.log.Timber
import java.io.IOException
import java.util.Calendar

class PendaftaranActivity : AppCompatActivity(), DatePickerDialog.OnDateSetListener {

    private val api by lazy { ApiService.getClient() }
    private val pref by lazy { Preferences(this) }
    private lateinit var viewModelFactory : LoginViewModelFactory
    private lateinit var viewModel : LoginViewModel
    private val binding by lazy { ActivityPendaftaranBinding.inflate(layoutInflater) }

    private var filePathPhoto3x4: Uri? = null
    private var filePathKK: Uri? = null
    private var filePathAkte: Uri? = null
    private var selectedJk: String = ""
    private var selectedAgama: String = ""

    var PERMISSIONS = arrayOf(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA
    )
    var PERMISSION_ALL = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        if (!hasPermissions(this, *PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }

        setupView()
        setupListener()
        setupObserver()
    }

    fun hasPermissions(context: Context, vararg permissions: String): Boolean = permissions.all {
        ActivityCompat.checkSelfPermission(context, it) == PackageManager.PERMISSION_GRANTED
    }

    private fun setupView() {

        viewModelFactory = LoginViewModelFactory( api, this )
        viewModel = ViewModelProvider(this, viewModelFactory).get(LoginViewModel::class.java)

        val jenisKelamin = listOf("","Laki-laki", "Perempuan")
        val agama = listOf("","Islam", "Kristen", "Budha", "Hindu", "Konghucu")

        binding.tglLahir.setOnClickListener {
            showDatePickerDialog()
        }

        val adapterJk = ArrayAdapter(this, android.R.layout.simple_spinner_item, jenisKelamin)
        adapterJk.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        val adapterAgama = ArrayAdapter(this, android.R.layout.simple_spinner_item, agama)
        adapterJk.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        binding.jenisKelamin.adapter = adapterJk
        binding.jenisKelamin.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectedJk = jenisKelamin[position]
                // Lakukan sesuatu dengan buah yang dipilih
                println("jk dipilih: $selectedJk")
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                // Tangani ketika tidak ada buah yang dipilih
            }
        }

        binding.agama.adapter = adapterAgama
        binding.agama.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectedAgama = agama[position]
                // Lakukan sesuatu dengan buah yang dipilih
                println("jk dipilih: $selectedAgama")
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                // Tangani ketika tidak ada buah yang dipilih
            }
        }

    }

    private fun showDatePickerDialog() {
        val calendar = Calendar.getInstance()
        val year = calendar.get(Calendar.YEAR)
        val month = calendar.get(Calendar.MONTH)
        val day = calendar.get(Calendar.DAY_OF_MONTH)

        val datePickerDialog = DatePickerDialog(this, this, year, month, day)
        datePickerDialog.show()
    }

    override fun onDateSet(view: DatePicker, year: Int, month: Int, dayOfMonth: Int) {
        // Di sini Anda dapat melakukan sesuatu dengan tanggal yang dipilih
        val selectedDate = Calendar.getInstance()
        selectedDate.set(Calendar.YEAR, year)
        selectedDate.set(Calendar.MONTH, month)
        selectedDate.set(Calendar.DAY_OF_MONTH, dayOfMonth)

        // Contoh: Menampilkan tanggal yang dipilih di logcat
        val formattedDate = android.text.format.DateFormat.format("yyyy-MM-dd", selectedDate)
        println("Tanggal dipilih: $formattedDate")
        binding.tglLahir.setText(formattedDate)
    }

    private fun setupListener() {

        binding.btnDaftar.setOnClickListener {

            val uriPathHelper = URIPathHelper()
            val file3x4 = uriPathHelper.getPath(this, filePathPhoto3x4!!)
            val filekk = uriPathHelper.getPath(this, filePathKK!!)
            val fileakte = uriPathHelper.getPath(this, filePathAkte!!)
            Log.i("file3x4", file3x4.toString())
            Log.i("filekk", filekk.toString())
            Log.i("fileakte", fileakte.toString())

            viewModel.daftarProses(
                binding.namaLengkap.text.toString(),
                binding.tglLahir.text.toString(),
                selectedJk,
                selectedAgama,
                binding.namaAyah.text.toString(),
                binding.noTelpAyah.text.toString(),
                binding.namaIbu.text.toString(),
                binding.noTelpIbu.text.toString(),
                binding.username.text.toString(),
                binding.password.text.toString(),
                file3x4.toString(),
                filekk.toString(),
                fileakte.toString()
            )
        }

        binding.uploadPhoto3x4.setOnClickListener {
            launchGallery(Constants.INTENT.PHOTO3X4)
        }

        binding.uploadKk.setOnClickListener {
            launchGallery(Constants.INTENT.KK)
        }

        binding.uploadAkte.setOnClickListener {
            launchGallery(Constants.INTENT.AKTE)
        }

        binding.icBack.setOnClickListener {
            finish()
        }

    }

    private fun launchGallery(code: Int) {

        val builder = AlertDialog.Builder(this)
        //set title for alert dialog
        builder.setTitle("Upload foto")
        //set message for alert dialog
        builder.setMessage("Silahkan pilih")
        builder.setIcon(android.R.drawable.ic_dialog_alert)

        //performing positive action
        builder.setPositiveButton("Camera"){dialogInterface, which ->
            ImagePicker.with(this)
                // User can only capture image from Camera
                .cameraOnly()
                // Image size will be less than 1024 KB
                .compress(1024)
                .start(code)
        }
        //performing cancel action
        builder.setNeutralButton("Cancel"){dialogInterface , which ->
            Toast.makeText(applicationContext,"clicked cancel\n operation cancel",Toast.LENGTH_LONG).show()
        }
        //performing negative action
        builder.setNegativeButton("Gallery"){dialogInterface, which ->

            ImagePicker.with(this)
                .galleryOnly()	//User can only select image from Gallery
                .start(code)	//Default Request Code is ImagePicker.REQUEST_CODE

        }
        // Create the AlertDialog
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == Constants.INTENT.PHOTO3X4 && resultCode == Activity.RESULT_OK) {
            if(data == null || data.data == null){
                return
            }

            filePathPhoto3x4 = data.data
            try {

                Glide
                    .with(this)
                    .load(filePathPhoto3x4)
                    .placeholder(R.drawable.ic_no_image)
                    .into(binding.hasilPhoto3x4);


                binding.icUploadPhoto3x4.isVisible = false
                binding.lblIcUploadPhoto3x4.isVisible = false
                binding.hasilPhoto3x4.isVisible = true

            } catch (e: IOException) {
                e.printStackTrace()
            }
        } else if (requestCode == Constants.INTENT.KK && resultCode == Activity.RESULT_OK) {
            if(data == null || data.data == null){
                return
            }

            filePathKK = data.data
            try {

                Glide
                    .with(this)
                    .load(filePathKK)
                    .placeholder(R.drawable.ic_no_image)
                    .into(binding.hasilKk);


                binding.icUploadKk.isVisible = false
                binding.lblIcUploadKk.isVisible = false
                binding.hasilKk.isVisible = true

            } catch (e: IOException) {
                e.printStackTrace()
            }
        } else if (requestCode == Constants.INTENT.AKTE && resultCode == Activity.RESULT_OK) {
            if(data == null || data.data == null){
                return
            }

            filePathAkte = data.data
            try {

                Glide
                    .with(this)
                    .load(filePathAkte)
                    .placeholder(R.drawable.ic_no_image)
                    .into(binding.hasilAkte);


                binding.icUploadAkte.isVisible = false
                binding.lblIcUploadAkte.isVisible = false
                binding.hasilAkte.isVisible = true

            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

    }

    private fun setupObserver() {
        viewModel.loginResponse.observe(this, Observer {
            when(it){
                is Resource.Loading -> {
                    LoadingScreen.displayLoadingWithText(this,"Silahkan Tunggu..",false)
                }
                is Resource.Success -> {
                    LoadingScreen.hideLoading()
                    Toast.makeText(this, "Pendaftaran berhasil !", Toast.LENGTH_SHORT).show()
                    val intent = Intent(this, LoginActivity::class.java)
                    startActivity(intent)
                    finish()

                }
                is Resource.Error -> {
                    LoadingScreen.hideLoading()
                    Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

}