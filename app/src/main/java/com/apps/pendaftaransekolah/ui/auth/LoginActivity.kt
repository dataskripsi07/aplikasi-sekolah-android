package com.apps.pendaftaransekolah.ui.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.apps.driverasrikatara.helper.LoadingScreen
import com.apps.pendaftaransekolah.api.ApiService
import com.apps.pendaftaransekolah.api.Resource
import com.apps.pendaftaransekolah.data.constants.Constants
import com.apps.pendaftaransekolah.data.preferences.Preferences
import com.apps.pendaftaransekolah.databinding.ActivityLoginBinding
import com.apps.pendaftaransekolah.ui.PendaftaranActivity
import com.apps.pendaftaransekolah.ui.home.HomeActivity
import id.rizmaulana.sheenvalidator.lib.SheenValidator
import timber.log.Timber

class LoginActivity : AppCompatActivity() {

    private val api by lazy { ApiService.getClient() }
    private val pref by lazy { Preferences(this) }
    private lateinit var viewModelFactory : LoginViewModelFactory
    private lateinit var viewModel : LoginViewModel
    private val binding by lazy { ActivityLoginBinding.inflate(layoutInflater) }
    private lateinit var sheenValidator: SheenValidator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setupViewModel()
        setupListener()
        setupObserver()
    }


    private fun setupViewModel() {
        viewModelFactory = LoginViewModelFactory( api, this )
        viewModel = ViewModelProvider(this, viewModelFactory).get(LoginViewModel::class.java)

        sheenValidator = SheenValidator(this)
        sheenValidator.setOnValidatorListener {
            viewModel.loginProses(binding.etUsername.text.toString(),binding.etPassword.text.toString())
        }
        sheenValidator.registerAsRequired(binding.etUsername)
        sheenValidator.registerAsRequired(binding.etPassword)

    }

    private fun setupListener() {
        binding.etUsername.setText(pref.getString(Constants.DEFAULT.USERNAME))
        binding.btnLogin.setOnClickListener {
            sheenValidator.validate()

        }
        binding.btnDaftar.setOnClickListener {
            val intent = Intent(this, PendaftaranActivity::class.java)
            startActivity(intent)
        }
    }

    private fun setupObserver() {
        viewModel.loginResponse.observe(this, Observer {
            when(it){
                is Resource.Loading -> {
                    LoadingScreen.displayLoadingWithText(this,"Silahkan Tunggu..",false)
                }
                is Resource.Success -> {
                    LoadingScreen.hideLoading()
                    Timber.e("TAG ${it.data!!.data}")
                    Timber.e("TAG ${it.data!!.data[0].token}")
                    Toast.makeText(this, "Berhasil login", Toast.LENGTH_SHORT).show()
                    pref.put(Constants.DEFAULT.TOKEN, it.data.data[0].token)
                    pref.put(Constants.DEFAULT.USERNAME,binding.etUsername.text.trim().toString())
                    pref.put(Constants.DEFAULT.USER_ID, it.data.data[0].id_user)
                    pref.put(Constants.DEFAULT.NAMA, it.data.data[0].nama_lengkap)
                    pref.put(Constants.DEFAULT.ID_PENDAFTARAN, it.data.data[0].id_pendaftaran)
                    pref.put(Constants.DEFAULT.LOGIN_NEW, "Y")

                    val intent = Intent(this, HomeActivity::class.java)
                    startActivity(intent)
                    finish()

                }
                is Resource.Error -> {
                    LoadingScreen.hideLoading()
                    Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

}