package com.apps.pendaftaransekolah.ui.home

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import com.apps.pendaftaransekolah.databinding.ActivityHomeBinding
import android.view.Menu
import android.view.MenuItem
import android.view.View

import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.apps.driverasrikatara.helper.LoadingScreen
import com.apps.pendaftaransekolah.R
import com.apps.pendaftaransekolah.api.ApiService
import com.apps.pendaftaransekolah.api.Resource
import com.apps.pendaftaransekolah.data.constants.Constants
import com.apps.pendaftaransekolah.data.preferences.Preferences
import com.apps.pendaftaransekolah.ui.auth.LoginViewModel
import com.apps.pendaftaransekolah.ui.auth.LoginViewModelFactory
import com.apps.pendaftaransekolah.ui.splash.SplashActivity
import timber.log.Timber


class HomeActivity : AppCompatActivity() {
    private val api by lazy { ApiService.getClient() }
    private val pref by lazy { Preferences(this) }
    private lateinit var viewModelFactory : HomeViewModelFactory
    private lateinit var viewModel : HomeViewModel
    private val binding by lazy { ActivityHomeBinding.inflate(layoutInflater) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        getSupportActionBar()?.setHomeButtonEnabled(true); //for back button
        //getSupportActionBar()?.setDisplayHomeAsUpEnabled(true);//for back button
        getSupportActionBar()?.setTitle(Html.fromHtml("<font color='#ffffff'>Pendaftaran Sekolah </font>"));

        setupViewModel()
        setupListener()
        setupObserver()
    }

    private fun setupViewModel() {
        viewModelFactory = HomeViewModelFactory( api, this )
        viewModel = ViewModelProvider(this, viewModelFactory).get(HomeViewModel::class.java)

        val id_user = pref.getString(Constants.DEFAULT.USER_ID)
        val id_pendaftaran = pref.getString(Constants.DEFAULT.ID_PENDAFTARAN)
        val nama = pref.getString(Constants.DEFAULT.NAMA)

        Timber.e("ID USER = ${id_user.toString()}")
        Timber.e("ID PENDF = ${id_pendaftaran.toString()}")

        binding.tvNama.text = nama.toString()

        viewModel.getKelulusan(id_user.toString(), id_pendaftaran.toString())

    }

    private fun setupListener() {

    }

    private fun setupObserver() {
        viewModel.kelulusanResp.observe(this, Observer {
            when(it){
                is Resource.Loading -> {
                    LoadingScreen.displayLoadingWithText(this,"Silahkan Tunggu..",false)
                }
                is Resource.Success -> {
                    LoadingScreen.hideLoading()
                    Timber.e("TAG ${it.data!!.data}")
                    binding.noPendaftaran.text = it.data.data[0].no_pendaftaran
                    binding.namaLengkap.text = it.data.data[0].nama
                    binding.namaAyah.text = it.data.data[0].nama_ayah
                    binding.namaIbu.text = it.data.data[0].nama_ibu
                    binding.tglDaftar.text = it.data.data[0].tgl_daftar
                    binding.periode.text = it.data.data[0].periode
                    binding.tglTes.text = it.data.data[0].tgl_tes
                    binding.adminUser.text = it.data.data[0].admin_user

                    if (it.data.data[0].lulus.equals("Lulus")) {
                        binding.lulus.visibility = View.VISIBLE
                    } else if (it.data.data[0].lulus.equals("Lulus Administrasi")) {
                        binding.lulusAdministrasi.visibility = View.VISIBLE
                    } else {
                        binding.proses.visibility = View.VISIBLE
                    }
                }
                is Resource.Error -> {
                    LoadingScreen.hideLoading()
                    Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_home, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id: Int = item.getItemId()
        if (id == R.id.action_profile) {
//            Toast.makeText(this, "Profile", Toast.LENGTH_SHORT).show()
            pref.delete()
            val intent = Intent(this, SplashActivity::class.java)
            startActivity(intent)
            finish()
            return true
        } else if (id == R.id.action_refresh) {
            Toast.makeText(this, "reload", Toast.LENGTH_SHORT).show()
            finish();
            startActivity(getIntent());
            return true
        }
        return super.onOptionsItemSelected(item)
    }

}