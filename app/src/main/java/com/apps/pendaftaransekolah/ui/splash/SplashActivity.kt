package com.apps.pendaftaransekolah.ui.splash

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.apps.pendaftaransekolah.R
import com.apps.pendaftaransekolah.data.constants.Constants
import com.apps.pendaftaransekolah.data.preferences.Preferences
import com.apps.pendaftaransekolah.databinding.ActivitySplashBinding
import com.apps.pendaftaransekolah.ui.auth.LoginActivity
import com.apps.pendaftaransekolah.ui.home.HomeActivity
import timber.log.Timber

class SplashActivity : AppCompatActivity() {

    private val viewModel by lazy { ViewModelProvider(this).get(SplashViewModel::class.java) }
    private val binding by lazy { ActivitySplashBinding.inflate(layoutInflater) }
    lateinit var token: String
    private val pref by lazy { Preferences(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN)
        window.statusBarColor = Color.TRANSPARENT

        val pref = Preferences(this)
        token = pref.getString(Constants.DEFAULT.TOKEN).toString()
        setupObserve()
    }

    private fun setupObserve() {
        viewModel.delayCountDown(2000)
        viewModel.splashNotifier.observe(this, Observer { finishSplash ->

            Timber.e("CekToken  : $token")

            if (token == "null") {
                startActivity(
                    Intent(this, LoginActivity::class.java)
                )
                finish()
            } else {
                pref.put(Constants.DEFAULT.LOGIN_NEW, "T")
                val intent = Intent(this, HomeActivity::class.java)
                startActivity(intent)
                finish()

            }

        })
    }
}