package com.apps.pendaftaransekolah.ui.auth

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.apps.pendaftaransekolah.R
import com.apps.pendaftaransekolah.api.ApiEndPoint
import com.apps.pendaftaransekolah.api.Resource
import com.apps.pendaftaransekolah.data.response.LoginResp
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class LoginViewModel (
    val api: ApiEndPoint,
    val context : Context
) : ViewModel() {

    val loginResponse: MutableLiveData<Resource<LoginResp>> = MutableLiveData()
    fun loginProses(username: String, password: String) = viewModelScope.launch {
        loginResponse.value = Resource.Loading()
        val dataApi = api.login(username = username, password = password)
        try {
            if (dataApi.code() == 401) {
                loginResponse.value = Resource.Error("Token Unauthorized atau akun sedang tidak aktif!")
            } else {
                if (dataApi.body()?.error.equals("true")) {
                    loginResponse.value = Resource.Success( dataApi.body()!! )
                } else {
                    loginResponse.value = Resource.Error( dataApi.body()?.message.toString() )
                }
            }

        } catch (e: Exception) {
            if(dataApi.code() == 500) {
                loginResponse.value = Resource.Error(context.getString(R.string.error_server))
            } else {
                loginResponse.value = Resource.Error( e.message.toString() )
            }
        }
    }

    fun daftarProses(
        nama: String, tgllahir:String, jk:String, agama:String, namaAyah: String, noayah:String, namaIbu: String, noibu: String, username: String, password: String, file3x4: String, fileKK: String, fileAKTE: String
    ) = viewModelScope.launch {
        loginResponse.value = Resource.Loading()

        var fileFoto3x4 = File(file3x4)
        var requestFile1 = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), fileFoto3x4)
        var bodyFile1 = MultipartBody.Part.createFormData("foto_3x4", fileFoto3x4.name, requestFile1)

        var fileFotoKK = File(fileKK)
        var requestFile2 = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), fileFotoKK)
        var bodyFile2 = MultipartBody.Part.createFormData("foto_kk", fileFotoKK.name, requestFile2)

        var fileFotoAKTE = File(fileAKTE)
        var requestFile3 = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), fileFotoAKTE)
        var bodyFile3 = MultipartBody.Part.createFormData("foto_akte", fileFotoAKTE.name, requestFile3)

        val dataApi = api.daftar(
            namaLengkap = nama,
            tglLahir = tgllahir,
            jk = jk,
            agama = agama,
            namaAyah = namaAyah,
            noayah = noayah,
            namaIbu = namaIbu,
            noibu = noibu,
            username = username,
            password = password,
            foto_3x4 = bodyFile1,
            foto_kk = bodyFile2,
            foto_akte = bodyFile3,
        )
        try {
            if (dataApi.code() == 401) {
                loginResponse.value = Resource.Error("Token Unauthorized atau akun sedang tidak aktif!")
            } else {
                if (dataApi.body()?.error.equals("true")) {
                    loginResponse.value = Resource.Success( dataApi.body()!! )
                } else {
                    loginResponse.value = Resource.Error( dataApi.body()?.message.toString() )
                }
            }

        } catch (e: Exception) {
            if(dataApi.code() == 500) {
                loginResponse.value = Resource.Error(context.getString(R.string.error_server))
            } else {
                loginResponse.value = Resource.Error( e.message.toString() )
            }
        }
    }

}