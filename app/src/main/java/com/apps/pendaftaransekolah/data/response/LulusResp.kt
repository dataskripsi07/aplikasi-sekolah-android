package com.apps.pendaftaransekolah.data.response

data class LulusResp(
    val error: String,
    val message: String,
    val `data`: List<Data>
) {
    data class Data(
        val no_pendaftaran: String,
        val nama: String,
        val nama_ayah: String,
        val nama_ibu: String,
        val tgl_daftar: String,
        val lulus: String,
        val tgl_tes: String,
        val admin_user: String,
        val periode: String
    )
}