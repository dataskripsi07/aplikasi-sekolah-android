package com.apps.pendaftaransekolah.data.preferences

import android.content.Context
import android.content.SharedPreferences

private const val prefName = "Pendaftaransekolah.pref"

class Preferences(context: Context) {
    private var sharedPref: SharedPreferences =
        context.getSharedPreferences(prefName, Context.MODE_PRIVATE)
    private val editor: SharedPreferences.Editor

    init {
        editor = sharedPref.edit()
    }

    fun put(key: String, value: String?) {
        editor.putString(key, value)
            .apply()
    }

    fun delete() {
        editor.clear()
        editor.commit()
    }

    fun getString(key: String): String? {
        return sharedPref.getString(key, null)
    }
}