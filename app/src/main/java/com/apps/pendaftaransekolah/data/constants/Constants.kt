package com.apps.pendaftaransekolah.data.constants

class Constants {

    object DEFAULT {
//        const val BASE_URL = "https://demo.ayogolive.my.id/pendaftaransekolah/"
        const val BASE_URL = "http://192.168.1.11/pendaftaran_sekolah/"
//        const val BASE_URL = "http://192.168.129.54"
        const val USER_ID = "auth_id_user"
        const val USERNAME = "username_user"
        const val TOKEN = "token_user"
        const val NAMA = "nama_user"
        const val ID_PENDAFTARAN = "nik_user"
        const val JK = "jk_user"
        const val NO_HP = "hp_user"
        const val EMAIL = "email_user"
        const val TEMPAT_LAHIR = "tl_user"
        const val TGL_LAHIR = "tgl_user"
        const val ALAMAT = "alamat_user"
        const val SALDO = "saldo_user"
        const val STATUS = "status_user"
        const val KTP = "ktp_user"
        const val FOTO = "selfie_user"
        const val SIM = "sim_user"
        const val LOGIN_NEW = "login_new"
    }

    object INTENT {
        const val KEY_ROLE = "role"
        const val PHOTO3X4 = 71
        const val KK = 72
        const val AKTE = 73
    }

}