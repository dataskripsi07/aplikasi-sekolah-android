package com.apps.pendaftaransekolah.data.response

data class LoginResp(
    val error: String,
    val message: String,
    val `data`: List<Data>
) {
    data class Data(
        val id_user: String,
        val username: String,
        val id_pendaftaran: String,
        val nama_lengkap: String,
        val token: String
    )
}