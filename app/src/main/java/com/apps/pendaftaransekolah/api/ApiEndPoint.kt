package com.apps.pendaftaransekolah.api

import com.apps.pendaftaransekolah.data.response.*
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.*


interface ApiEndPoint {

    @FormUrlEncoded
    @POST("api/login")
    suspend fun login(
        @Field("username") username: String?,
        @Field("password") password: String?,
    ): Response<LoginResp>

    @FormUrlEncoded
    @POST("api/get_pendaftaran")
    suspend fun getKelulusan(
        @Field("id_user") idUser: String?,
        @Field("id_pendaftaran") idPendaftaran: String?,
    ): Response<LulusResp>

    @Multipart
    @POST("api/daftar")
    suspend fun daftar(
        @Part("nama_lengkap") namaLengkap: String?,
        @Part("tanggal_lahir") tglLahir: String?,
        @Part("jenis_kelamin") jk: String?,
        @Part("agama") agama: String?,
        @Part("nama_ayah") namaAyah: String?,
        @Part("no_telp_ayah") noayah: String?,
        @Part("nama_ibu") namaIbu: String?,
        @Part("no_telp_ibu") noibu: String?,
        @Part("username") username: String?,
        @Part("password") password: String?,
        @Part foto_3x4: MultipartBody.Part?,
        @Part foto_kk: MultipartBody.Part?,
        @Part foto_akte: MultipartBody.Part?,
    ): Response<LoginResp>


}